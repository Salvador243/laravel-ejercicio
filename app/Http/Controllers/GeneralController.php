<?php

namespace App\Http\Controllers;

use App\Models\Opciones;
use App\Models\Preguntas;
use App\Models\Test;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GeneralController extends Controller
{
    public function home()
    {
        // regresar el rol y retornar los primeros 10 test
        $user = Auth::user();
        $rol = $user->roles->first()->nombre;

        if ($rol == 'administrador') {
            $tests = Test::paginate(10);
        } else {
            $tests = $user->tests()->paginate(10);
        }

        return view('dashboard', ['user' => $user, 'tests' => $tests]);
    }

    public function getPreguntas($id)
    {
        // obtener preguntas relacionadas al test del que el usuario tiene acceso
        $test = Test::where('id', '=', $id)->first();
        $preguntas = Preguntas::where('test_id', '=', $id)->get();

        $usuarios = User::with('tests')->get();

        $usuariosSinTests = User::whereDoesntHave('tests')->get();
        return view('preguntas', [
            'preguntas'         => $preguntas,
            'usuarios'          => $usuarios,
            'usuariosSinTests'  => $usuariosSinTests,
            'test'              => $test,
        ]);
    }

    public function getOpciones($id)
    {
        // obtener las opciones de las preguntas en base al id de la pregunta
        $opciones = Opciones::where('pregunta_id', '=', $id)->get();
        $pregunta = Preguntas::where('id', '=', $id)->first();
        return view('opciones', [
            'opciones' => $opciones,
            'pregunta' => $pregunta
        ]);
    }

    public function ajaxAcceso(Request $r)
    {
        // dar  o  quitar acceso a un usuario siendo administrador
        $user = User::find($r->input('id'));
        $test = Test::find($r->input('test'));
        if ($r->input('acceso') == '1') {
            $user->tests()->detach($test->id);
        } else {
            $user->tests()->attach($test->id);
        }
    }

    public function guardarTest(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($r->input('id')) {
            // editar nuevo test
            $test = Test::findOrFail(intval($r->input('id')));
        } else {
            // crear nuevo test
            $test = new Test();
        }
        $test->nombre = $r->input('nombre');
        $test->descripcion = $r->input('descripcion');
        $test->save();

        return redirect('/home');
    }

    public function editarTest($id)
    {
        // obtener informacion del test que se quiere editar
        $informacion = Test::where('id', '=', $id)->get();
        return $informacion[0];
    }

    public function eliminarTest($id)
    {
        // se elimina al test seleccionado
        $test = Test::findOrFail($id);
        $test->delete();
    }

    public function editarPregunta($id)
    {
        // obtener la informacion de ña pregunta que se editara para precargar la informacion
        $informacion = Preguntas::where('id', '=', $id)->get();
        return $informacion[0];
    }

    public function guardarPregunta(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'pregunta' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        //guardar  pregunta editada
        $id = $r->input('test_id');
        if ($r->input('id')) {
            $pregunta = Preguntas::findOrFail(intval($r->input('id')));
        } else {
            // crear una pregunta nueva
            $pregunta = new Preguntas();
        }
        $pregunta->pregunta = $r->input('pregunta');
        $pregunta->test_id = $id;
        $pregunta->save();
        return redirect("/pregunta/" . $id);
    }

    public function eliminarPregunta(Request $r)
    {
        // eliminar la pregunta
        $id = $r->input('id');
        $test_id = $r->input('test_id');

        $pregunta = Preguntas::findOrFail($id);
        $pregunta->delete();

        return redirect("/pregunta/" . $test_id);
    }

    public function guardarOpcion(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'opcion' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $id = $r->input('id');

        if ($r->input('id')) {
            // actualizar la opcion
            $opcion = Opciones::findOrFail($id);
        } else {
            // crear una nueva opcion
            $opcion = new Opciones();
        }
        $opcion->pregunta_id = $r->input('pregunta_id');
        $opcion->opcion = $r->input('opcion');
        $opcion->save();

        return redirect("/opcion/" . $r->input('pregunta_id'));
    }

    public function editarOpcion($id)
    {
        // obtener la informacion de la opcion a editar
        $informacion = Opciones::where('id', '=', $id)->get();
        return $informacion[0];
    }

    public function eliminarOpcion(Request $r)
    {
        // eliminar la opcion seleccionada
        $id = $r->input('id');
        $pregunta_id = $r->input('pregunta_id');

        $opcion = Opciones::findOrFail($id);
        $opcion->delete();

        return redirect("/opcion/" . $pregunta_id);
    }

    public function usuarios()
    {
        // se obtienen todos los usuarios para la vista
        $usuarios = User::query()
            ->orderBy('name', 'asc')
            ->get();
        return view('usuarios', [
            'usuarios' => $usuarios
        ]);
    }

    public function actualizarPassword(Request $r, $id)
    {
        $validator = Validator::make($r->all(), [
            'password' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // se actulizan los datos del usuario
        $password = $r->input('password');
        $name = $r->input('name');
        $email = $r->input('email');

        $usuario = User::findOrFail($id);
        $usuario->password = bcrypt($password);
        $usuario->name = $name;
        $usuario->email = $email;

        $usuario->save();

        return redirect('/usuarios');
    }

    public function cargarPreguntas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'preguntas_csv' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // leer el archivo csv que se carga
        $archivo = $request->file('preguntas_csv');
        $path = $archivo->store('temp');
        $csv = Storage::get($path);
        $filas = str_getcsv($csv, "\n");
        $test_id = $request->input('test_id');

        foreach ($filas as $fila) {
            $datos = str_getcsv($fila, ",");
            $pregunta = $datos[0];
            $opciones = array_slice($datos, 1);
            if ($fila) {
                // se crea la pregunta y se relaciona al test
                $pregunta_modelo = new Preguntas([
                    'test_id' => $test_id,
                    'pregunta' => $pregunta
                ]);
                $pregunta_modelo->save();
                foreach ($opciones as $opcion) {
                    // se insertan la opciones de la pregunta agregada
                    $pregunta_modelo->opciones()->create(['opcion' => $opcion]);
                }
            }
        }
        // eliminar el archivo
        Storage::delete($path);
        return redirect('/pregunta/' . $test_id);
    }
}
