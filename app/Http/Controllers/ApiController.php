<?php

namespace App\Http\Controllers;

use App\Models\Preguntas;
use App\Models\Test;
use App\Models\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    // Funciones para la API
    public function getTests()
    {
        $tests = Test::all();
        return response()->json($tests);
    }
    // Obtener usuario por usuario
    public function getUser($id)
    {
        $user = User::where('id', '=', $id)->first();
        return response()->json($user);
    }
    // Obtener preguntas por id de test
    public function getPregunta($id)
    {
        $user = Test::find($id);
        return response()->json($user->preguntas);
    }
    // Obtener opciones por id de pregunta
    public function getOpcion($id)
    {
        $user = Preguntas::find($id);
        return response()->json($user->opciones);
    }
}
