<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class Log extends Controller
{
    public function login(Request $r){
        $email = $r->input('email');
        $password = $r->input('password');
        if(User::autenticar($email, $password)){
            return redirect('/home');
        }else{
            return view('/welcome');
        }
    }
}
