<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Test extends Model
{
    protected $table = "tests";

    use HasApiTokens, HasFactory, Notifiable;

    public function preguntas()
    {
        return $this->hasMany(Preguntas::class, 'test_id');
    }
}
