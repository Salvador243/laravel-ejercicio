<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Preguntas extends Model
{
    use HasFactory;
    protected $fillable = [
        'test_id',
        'pregunta',
        'tipo',
        'estatus',
    ];
    public function opciones()
    {
        return $this->hasMany(Opciones::class, "pregunta_id");
    }
}
