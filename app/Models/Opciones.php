<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opciones extends Model
{
    use HasFactory;
    protected $fillable = [
        'opcion',
    ];
    public function pregunta()
    {
        return $this->belongsTo(Preguntas::class, 'pregunta_id');
    }
}
