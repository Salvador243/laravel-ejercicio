<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Crear usuario de prueba
        $user = \App\Models\User::factory()->create([
            'name' => 'John Doe',
            'email' => 'salva@es.es',
            'password' => bcrypt('123456'),
        ]);

        $user2 = \App\Models\User::factory()->create([
            'name' => 'Usuario 2',
            'email' => 'user2@es.es',
            'password' => bcrypt('123456789'),
        ]);
        $user3 = \App\Models\User::factory()->create([
            'name' => 'Usuario 4',
            'email' => 'user4@es.es',
            'password' => bcrypt('123456789'),
        ]);

        // crear roles
        $role1 = \App\Models\Roles::factory()->create([
            'nombre' => 'administrador'
        ]);

        $role2 = \App\Models\Roles::factory()->create([
            'nombre' => 'usuario'
        ]);
        // asociar el rol del usuario creado
        $user->roles()->attach($role1);
        $user2->roles()->attach($role2);
        $user3->roles()->attach($role2);
        // Crear dos tests de prueba
        $test1 = \App\Models\Test::create([
            'nombre' => 'Test 1',
            'descripcion' => 'This is a test about...'
        ]);

        $test2 = \App\Models\Test::create([
            'nombre' => 'Test 2',
            'descripcion' => 'This is another test about...'
        ]);

        // Asignar el primer test al usuario
        $user->tests()->attach($test1->id);
        $user2->tests()->attach($test2->id);

        // Crear preguntas para el primer test
        $pregunta1 = \App\Models\Preguntas::factory()->create([
            'test_id' => $test1->id,
            'pregunta' => 'What is the capital of France?',
            'tipo' => 'multiple_choice',
        ]);

        $pregunta1->opciones()->createMany([
            ['opcion' => 'Paris', 'es_correcta' => true, 'pregunta_id' => $pregunta1->id],
            ['opcion' => 'London', 'es_correcta' => false, 'pregunta_id' => $pregunta1->id],
            ['opcion' => 'Berlin', 'es_correcta' => false, 'pregunta_id' => $pregunta1->id],
            ['opcion' => 'Madrid', 'es_correcta' => false, 'pregunta_id' => $pregunta1->id],
        ]);

        $pregunta2 = \App\Models\Preguntas::factory()->create([
            'test_id' => $test1->id,
            'pregunta' => 'What is the largest planet in our solar system?',
            'tipo' => 'multiple_choice',
        ]);
        $pregunta2->opciones()->createMany([
            ['opcion' => 'Earth', 'es_correcta' => false, 'pregunta_id' => $pregunta2->id],
            ['opcion' => 'Mars', 'es_correcta' => false, 'pregunta_id' => $pregunta2->id],
            ['opcion' => 'Jupiter', 'es_correcta' => true, 'pregunta_id' => $pregunta2->id],
            ['opcion' => 'Saturn', 'es_correcta' => false, 'pregunta_id' => $pregunta2->id],
        ]);

        // Crear preguntas para el segundo test
        $pregunta3 = \App\Models\Preguntas::factory()->create([
            'test_id' => $test2->id,
            'pregunta' => 'What is the highest mountain in the world?',
            'tipo' => 'multiple_choice',
        ]);
        $pregunta3->opciones()->createMany([
            ['opcion' => 'Kilimanjaro', 'es_correcta' => false, 'pregunta_id' => $pregunta3->id],
            ['opcion' => 'Everest', 'es_correcta' => true, 'pregunta_id' => $pregunta3->id],
            ['opcion' => 'Denali', 'es_correcta' => false, 'pregunta_id' => $pregunta3->id],
            ['opcion' => 'Fuji', 'es_correcta' => false, 'pregunta_id' => $pregunta3->id],
        ]);

        $pregunta4 = \App\Models\Preguntas::factory()->create([
            'test_id' => $test2->id,
            'pregunta' => 'What is the largest ocean in the world?',
            'tipo' => 'multiple_choice',
        ]);
        $pregunta4->opciones()->createMany([
            ['opcion' => 'Atlantic', 'es_correcta' => false, 'pregunta_id' => $pregunta4->id],
            ['opcion' => 'Indian', 'es_correcta' => false, 'pregunta_id' => $pregunta4->id],
            ['opcion' => 'Arctic', 'es_correcta' => false, 'pregunta_id' => $pregunta4->id],
            ['opcion' => 'Pacific', 'es_correcta' => true, 'pregunta_id' => $pregunta4->id],
        ]);
    }
}
