@extends('base.base')
@section('contenido')
<div class="container">

    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading"></h4>
        <h4><span class="badge bg-secondary">{{$user->name}}</span></h4>
        <h5><span class="badge bg-secondary">{{ $user->email }}</span></h5>
        <h6><span class="badge bg-secondary">{{Auth::user()->roles->first()->nombre}}</span></h6>

        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            Llene todos los campos
        </div>
        @endif
        @if(Auth::user()->roles->first()->nombre == 'administrador')
        <hr>
        <button type="button" class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Crear Test
        </button>
        <a href="{{route('usuarios')}}" type="button" class="btn btn-outline-primary">
            Usuarios
        </a>
        @endif
    </div>
    <hr>
    <div class="row">
        @foreach ($tests as $test)
        <div class="col-4 p-3">
            <div class="card">
                <div class="card-title">
                    <h4 class="p-2">{{ $test->nombre }}</h4>
                </div>
                <div class="card-body">
                    <h5>{{ $test->descripcion }}</h5>
                </div>
                <div class="card-footer">

                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <a href="{{ route('pregunta', ['id' => $test->id]) }}" class="btn btn-warning">Ver preguntas</a>
                        @if(Auth::user()->roles->first()->nombre == 'administrador')
                        <button type="button" class="btn btn-dark cjs-editar" data-test="{{$test->id}}"
                            data-bs-toggle="modal" data-bs-target="#editarModal">
                            Editar Test
                        </button>
                        <button type="button" class="btn btn-danger cjs-eliminar" data-test="{{$test->id}}">
                            Eliminar Test
                        </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

{{-- Modal de creacion y edicion de test --}}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Nuevo Test</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{route('test')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" name="nombre">
                    <label for="">Descripción</label>
                    <input type="text" class="form-control" name="descripcion">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Crear</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editarModal" tabindex="-2" aria-labelledby="editarModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="editarModalLabel">Editar Test</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{route('test')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="text" class="form-control" name="id" id="id" hidden>
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" name="nombre" id="nombre">
                    <label for="">Descripción</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Crear</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{ $tests->links() }}
<script>
    $('.cjs-editar').on('click', function(){
        let test_id = $(this).data('test');
        $.ajax({
            url: "{{route('eTest','test_id')}}".replace("test_id", test_id),
            method: "GET",
            data: {}
        }).done( res=>{
            $("#nombre").val(res.nombre);
            $("#id").val(res.id);
            $("#descripcion").val(res.descripcion);
        });
    });

    $('.cjs-eliminar').on('click', function(){
        let test_id = $(this).data('test');
        $.ajax({
            url: "{{route('bTest','test_id')}}".replace("test_id", test_id),
            method: "GET",
            data: {}
        }).done( res=>{
            window.location.href = "{{ route('home') }}";
        });
    });
</script>
@endsection