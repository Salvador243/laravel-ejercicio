@extends('base.base')
@section('contenido')
<div class="container">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">{{ $pregunta->pregunta }}</h4>
        @if(Auth::user()->roles->first()->nombre == 'administrador')
        <hr>
        <button type="button" class="btn btn-outline-dark" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Crear opcion
        </button>
        @endif
    </div>
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        Llene todos los campos
    </div>
    @endif
    <hr>
    <div class="row">
        @if(count($opciones) > 0)
        @foreach ($opciones as $opcion)
        <div class="col-4 p-3">
            <div class="card">
                <div class="card-title">
                    <h4 class="p-2">{{ $opcion->opcion }}</h4>
                </div>
                <div class="card-footer">
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        @if(Auth::user()->roles->first()->nombre == 'administrador')
                        <button type="button" class="btn btn-dark cjs-editar" data-opcion="{{$opcion->id}}"
                            data-bs-toggle="modal" data-bs-target="#editarModal">
                            Editar Opción
                        </button>
                        <form action="{{route('bOpcion')}}" method="POST">
                            @csrf
                            @method("DELETE")
                            <input type="text" name="id" value="{{$opcion->id}}" hidden>
                            <input type="text" name="pregunta_id" value="{{ $pregunta->id }}" hidden>
                            <button type="submit" class="btn btn-danger cjs-eliminar">
                                Eliminar Opción
                            </button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <h3>No hay opciones creadas</h3>
        @endif
    </div>

    {{-- Modal de creacion y edicion de test --}}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Nueva Opcion</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{route('gOpcion')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <label for="">Opcion</label>
                        <input type="text" class="form-control" name="opcion">
                        <input type="text" name="pregunta_id" value="{{$pregunta->id}}" hidden>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Crear</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editarModal" tabindex="-2" aria-labelledby="editarModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="editarModalLabel">Editar Test</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{route('gOpcion')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <input type="text" class="form-control" name="id" id="id" hidden>
                        <label for="">Opcion</label>
                        <input type="text" class="form-control" name="opcion" id="opcion">
                        <input type="text" name="pregunta_id" value="{{$pregunta->id}}" hidden>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Crear</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('.cjs-editar').on('click', function(){
        let opcion_id = $(this).data('opcion');
        $.ajax({
            url: "{{route('eOpcion','opcion_id')}}".replace("opcion_id", opcion_id),
            method: "GET",
            data: {}
        }).done( res=>{
            $("#opcion").val(res.opcion);
            $("#id").val(res.id);
        });
    });
</script>
@endsection