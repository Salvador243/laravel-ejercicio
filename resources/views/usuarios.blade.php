@extends('base.base')
@section('contenido')
<div class="container mt-5">
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        Llene todos los campos
    </div>
    @endif
    <table class="table table-success table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Email</th>
                <th scope="col">Contraseña</th>
                <th scope="col">Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($usuarios as $usuario)
            <form action="{{route('actualizar', $usuario->id)}}" method="POST">
                @csrf
                @method('put')
                <tr>
                    <th scope="row">{{$usuario->id}}</th>
                    <td>
                        <input type="text" class="form-control" name="name" value="{{$usuario->name}}">
                    </td>
                    <td>
                        <input type="email" class="form-control" name="email" value="{{$usuario->email}}">
                    </td>
                    <td>
                        <input type="password" class="form-control" name="password">
                    </td>
                    <td>
                        <button type="submit" class="btn btn-secondary">Actualizar</button>
                    </td>
                </tr>
            </form>
            @endforeach
        </tbody>

    </table>
</div>
@endsection