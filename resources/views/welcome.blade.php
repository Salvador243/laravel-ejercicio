@extends('base.base')

@section('contenido')
<div class="container">
    <form action="{{ route('in') }}" method="POST">
        @CSRF
        <input type="text" class="form-control mt-3" name="email" placeholder="Correo electronico">
        <input type="password" class="form-control my-3" name="password" placeholder="Contraseña">
        <button class="btn btn-success" type="submit">Inciar sesion</button>
    </form>
</div>
@endsection