@extends('base.base')
@section('contenido')
<div class="container">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">{{ $test->nombre }}</h4>
        @if(Auth::user()->roles->first()->nombre == 'administrador')
        <hr>
        <p>{{ $test->descripcion }}</p>
        <button type="button" class="btn btn-outline-dark" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Crear pregunta
        </button>
        @endif
    </div>
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        Llene todos los campos o cargue un archivo CSV
    </div>
    @endif
    <hr class="my-4">
    <form method="POST" action="{{ route('cargar') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-3">
                <h4><span class="badge bg-secondary mb-3">Preguntas relacionadas al test</span></h4>
            </div>
            @if(Auth::user()->roles->first()->nombre == 'administrador')
            <div class="col-md-6">
                <input class="form-control form-control-sm" id="formFileSm" type="file" name="preguntas_csv"
                    accept=".csv">
                <input type="hidden" name="test_id" value="{{$test->id}}">
            </div>
            <div class="col-md-3">
                <button class="btn btn-success" type="submit">Cargar preguntas</button>
            </div>
            @endif
        </div>
    </form>
    <div class="row">
        @if(count($preguntas) > 0)
        @foreach ($preguntas as $pregunta)
        <div class="col-4 p-3">
            <div class="card">
                <div class="card-title">
                    <h4 class="p-2">{{ $pregunta->pregunta }}</h4>
                </div>
                <div class="card-footer">
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <a href="{{ route('opcion', ['id' => $pregunta->id]) }}" class="btn btn-warning">Ver
                            opciones</a>
                        @if(Auth::user()->roles->first()->nombre == 'administrador')
                        <button type="button" class="btn btn-dark cjs-editar" data-pregunta="{{$pregunta->id}}"
                            data-bs-toggle="modal" data-bs-target="#editarModal">
                            Editar Pregunta
                        </button>
                        <form action="{{route('bPregunta')}}" method="POST">
                            @csrf
                            @method("DELETE")
                            <input type="text" name="id" value="{{$pregunta->id}}" hidden>
                            <input type="text" name="test_id" value="{{ $test->id }}" hidden>
                            <button type="submit" class="btn btn-danger cjs-eliminar">
                                Eliminar Pregunta
                            </button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <h3>No hay preguntas creadas</h3>
        @endif
    </div>
    @if(Auth::user()->roles->first()->nombre == 'administrador')
    <hr class="my-4">
    <div class="row">
        <h4><span class="badge bg-secondary mb-3">Usuarios con acceso al test</span></h4>
        <table class="table table-bordered border-primary">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre usuario</th>
                    <th scope="col">Correo usuario</th>
                    <th scope="col">Permiso</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios as $usuario)
                <tr>
                    <th scope="row">{{ $usuario->id }}</th>
                    <td>{{ $usuario->name }}</td>
                    <td>{{ $usuario->email }}</td>
                    @if(Auth::user()->roles->first()->nombre == 'administrador')
                    <td>
                        @php
                        $asignar = false;
                        foreach ($usuario->tests as $permiso) {
                        if ($permiso->pivot->test_id == $test->id) {
                        $asignar = true;
                        break;
                        }
                        }
                        @endphp
                        @if (!$asignar)
                        @if($permiso->pivot->user_id == Auth::user()->id && Auth::user()->roles->first()->nombre ==
                        'administrador')
                        <a data-id="{{ $usuario->id }}" class="cjs-acceso btn btn-dark" disabled>Adminstrador</a>
                        @else
                        <a data-id="{{ $usuario->id }}" data-acceso="2" data-test="{{$test->id}}"
                            class="cjs-acceso btn btn-success">Permitir
                            acceso</a>
                        @endif
                        @else
                        @if($permiso->pivot->user_id == Auth::user()->id && Auth::user()->roles->first()->nombre ==
                        'administrador')
                        <a data-id="{{ $usuario->id }}" class="cjs-acceso btn btn-dark" disabled>Adminstrador</a>
                        @else
                        <a data-id="{{ $usuario->id }}" data-acceso="1" data-test="{{$test->id}}"
                            class="cjs-acceso btn btn-danger">Remover
                            acceso</a>
                        @endif
                        @endif
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endif
</div>

{{-- Modal de creacion y edicion de test --}}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Nueva pregunta</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{route('gPregunta')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <label for="">Pregunta</label>
                    <input type="text" class="form-control" name="pregunta">
                    <input type="text" name="test_id" value="{{$test->id}}" hidden>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Crear</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editarModal" tabindex="-2" aria-labelledby="editarModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="editarModalLabel">Editar Pregunta</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{route('gPregunta')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="text" class="form-control" name="id" id="id" hidden>
                    <label for="">Pregunta</label>
                    <input type="text" class="form-control" name="pregunta" id="pregunta">
                    <input type="text" name="test_id" value="{{$test->id}}" hidden>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Crear</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $('.cjs-acceso').on('click', function() {
        let id = $(this).data('id');
        let acceso = $(this).data('acceso'); //1.Remover 2.Permitir
        let test = $(this).data('test');

        cambiarAcceso(id, acceso, test);
    });

    $('.cjs-editar').on('click', function(){
        let pregunta_id = $(this).data('pregunta');
        $.ajax({
            url: "{{route('ePregunta','pregunta_id')}}".replace("pregunta_id", pregunta_id),
            method: "GET",
            data: {}
        }).done( res=>{
            $("#pregunta").val(res.pregunta);
            $("#id").val(res.id);
        });
    });

    function cambiarAcceso(id, acceso, test) {
        $.ajax({
            url: "{{ route('acceso') }}",
            method: "POST",
            data: {
                id: id,
                acceso: acceso,
                test: test
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(() => {
            window.location.href = "{{ route('home') }}";
        });
    }
</script>
@endsection