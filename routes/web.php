<?php

use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Log;
use App\Http\Controllers\GeneralController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [GeneralController::class, 'home'])->name('home');

Route::get('/usuarios', [GeneralController::class, 'usuarios'])->name('usuarios');

Route::POST('/login', [Log::class, 'login'])->name('in');

Route::get('pregunta/{id}', [GeneralController::class, 'getPreguntas'])->name('pregunta');

Route::get('opcion/{id}', [GeneralController::class, 'getOpciones'])->name('opcion');

Route::post('/ajaxAcceso', [GeneralController::class, 'ajaxAcceso'])->name('acceso');

Route::post('/guardarTest', [GeneralController::class, 'guardarTest'])->name('test');

Route::get('/editarTest/{id}', [GeneralController::class, 'editarTest'])->name('eTest');

Route::get('/eliminarTest/{id}', [GeneralController::class, 'eliminarTest'])->name('bTest');

Route::get('/editarPregunta/{id}', [GeneralController::class, 'editarPregunta'])->name('ePregunta');

Route::post('/guardarPregunta', [GeneralController::class, 'guardarPregunta'])->name('gPregunta');

Route::post('/guardarOpcion', [GeneralController::class, 'guardarOpcion'])->name('gOpcion');

Route::delete('/eliminarPregunta', [GeneralController::class, 'eliminarPregunta'])->name('bPregunta');

Route::get('/editarOpcion/{id}', [GeneralController::class, 'editarOpcion'])->name('eOpcion');

Route::delete('/eliminarOpcion', [GeneralController::class, 'eliminarOpcion'])->name('bOpcion');

Route::put('/actualizarPassword/{id}', [GeneralController::class, 'actualizarPassword'])->name('actualizar');

Route::post('/cargar', [GeneralController::class, 'cargarPreguntas'])->name('cargar');

Route::get('/getTests', [ApiController::class, 'getTests']);

Route::get('/getUser/{id}', [ApiController::class, 'getUser']);

Route::get('/getPregunta/{id}', [ApiController::class, 'getPregunta']);

Route::get('/getOpcion/{id}', [ApiController::class, 'getOpcion']);
